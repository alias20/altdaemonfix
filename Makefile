DEBUG=0
FINALPACKAGE=1

THEOS_PACKAGE_SCHEME = rootless

TARGET = iphone:14.5:14.5
ARCHS = arm64
INSTALL_TARGET_PROCESSES = AltStore

THEOS_DEVICE_IP = 127.0.0.1 -p 2222

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = AltDaemonFix

AltDaemonFix_FILES = Tweak.x
AltDaemonFix_CFLAGS = -fobjc-arc
AltDaemonFix_LIBRARIES = sandy

include $(THEOS_MAKE_PATH)/tweak.mk
