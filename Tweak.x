#import <substrate.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <libSandy.h>

%ctor {
	@autoreleasepool {
		NSLog(@"[AltDaemonFix] Successfully Loaded.");

		libSandy_applyProfile("AltDaemon_MachAccess");
	}
}